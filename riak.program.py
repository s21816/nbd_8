import riak

# point 1 - store document
client = riak.RiakClient() # Connect to Riak.
bucket = client.bucket('s21816') # Choose the bucket to store data in.
road_data = {"distance":15,"name":"Rammam-Sabargram","summer":"true","winter":"true"}
road = bucket.new('road_10', data = road_data) # key and document to store
road.store() # Save the object to Riak.
print("we store this data : ", road_data)

#point 2 - get document and print it
road_10 = bucket.get('road_10')
print("we get from riak this data : ", road_10.data)

#point 3 - change document and save on RIAK (actually distance)
road_10.data['distance'] = 12
road = bucket.new('road_10', data=road_10.data) # key and document to store
road.store()
print("we update riak with this data : ", road_10.data)

#point 4 - get  updated document and print it
road_10 = bucket.get('road_10')
print("we get from riak this data : ", road_10.data)

#point 5 - delete document
bucket.delete('road_10')
print("document was deleted: ")

#point 6 - print deleted document
road_10 = bucket.get('road_10')
print("we get from riak this data : ", road_10.data)